import React, { Component } from 'react';
import axios from 'axios';


class Create extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeBusinessName = this.onChangeBusinessName.bind(this);
        this.onChangeGstNumber = this.onChangeGstNumber.bind(this);
        this.state = {
            person_name: '',
            business_name: '',
            gst_number: ''
        }
    }

    onChangeName(e) {
        this.setState({ name: e.target.value });
        console.log(this.state.name);
    }

    onChangeBusinessName(e) {
        this.setState({ business_name: e.target.value }); 
        console.log(this.state.business_name);

    }

    onChangeGstNumber(e) {
        this.setState({ gst_number: e.target.value }); 
        console.log(this.state.gst_number);

    }

    onSumbit(e) {
        e.preventDefault();
        console.log(`The values are ${this.state.name}, ${this.state.business_name}, and ${this.state.gst_number}`)

        const obj = {
            name: this.state.name,
            business_name:this.state.business_name,
            gst_number:this.state.gst_number
        }

        axios.post('http://localhost:3000/users/add', obj)
        .then(res => console.log(res.data));

        this.setState({
            name: '',
            business_name: '',
            gst_number: ''
        })

    }

    render() {
        return (
            <div style={{ marginTop: 10 }} >
                <h3>Add New Business</h3>
                <form onSubmit={this.onSumbit}>
                    <div className="form-group">
                        <label>Add Person Name:  </label>
                        <input type="text" className="form-control" value={this.state.name} onChange={this.onChangeName}/>
                    </div>
                    <div className="form-group">
                        <label>Add Business Name: </label>
                        <input type="text" className="form-control" value={this.state.business_name} onChange={this.onChangeBusinessName} />
                    </div>
                    <div className="form-group">
                        <label>Add GST Number: </label>
                        <input type="text" className="form-control" value={this.state.gst_number} onChange={this.onChangeGstNumber} />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Register Business" className="btn btn-primary" />
                    </div>
                </form>
            </div >
        )
    }
}

export default Create;
