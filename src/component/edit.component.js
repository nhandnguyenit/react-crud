import React, { Component } from 'react';
import axios from 'axios';

class Edit extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeBusinessName = this.onChangeBusinessName.bind(this);
        this.onChangeGstNumber = this.onChangeGstNumber.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            name: '',
            business_name: '',
            gst_number: ''
        }
    }

    componentDidMount(){
        axios.get('http://localhost:3000/users/'+this.props.match.params.id)
        .then(res => {
            // console.log(res.data);
            this.setState({
                name: res.data.name,
                business_name: res.data.business_name,
                gst_number: res.data.gst_number,
            });
            // console.log(this.state)
        }).catch(function(error){
            console.log(error);
        })
    }

    onSubmit(e){
        console.log(this.state)
        e.preventDefault();
        const obj = {
            name: this.state.name,
            business_name: this.state.business_name,
            gst_number: this.state.gst_number
        };

        axios.put('http://localhost:3000/users/'+this.props.match.params.id, obj)
        .then(res => console.log(res.data));

        this.props.history.push('/index');
    }

    onChangeName(e) {
        this.setState({ name: e.target.value });
        console.log(this.state.name);
    }

    onChangeBusinessName(e) {
        this.setState({ business_name: e.target.value });
        console.log(this.state.business_name);

    }

    onChangeGstNumber(e) {
        this.setState({ gst_number: e.target.value });
        console.log(this.state.gst_number);

    }

    render() {
        return (
            <div style={{ marginTop: 10 }}>
                <h3 align="center">Update Business</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Person Name:  </label>
                        <input
                            type="text"
                            className="form-control"
                            value={this.state.name}
                            onChange={this.onChangeName}
                        />
                    </div>
                    <div className="form-group">
                        <label>Business Name: </label>
                        <input type="text"
                            className="form-control"
                            value={this.state.business_name}
                            onChange={this.onChangeBusinessName}
                        />
                    </div>
                    <div className="form-group">
                        <label>GST Number: </label>
                        <input type="text"
                            className="form-control"
                            value={this.state.gst_number}
                            onChange={this.onChangeGstNumber}
                        />
                    </div>
                    <div className="form-group">
                        <input type="submit"
                            value="Update User"
                            className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}

export default Edit;
