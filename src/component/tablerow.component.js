import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Axios from 'axios';

class TableRow extends Component {
  constructor(props){
    super(props);
    console.log(this.props);
    this.state = {age: 12};
    console.log(this.state);
    this.delete = this.delete.bind(this);
  }

  delete(){
    Axios.delete('http://localhost:3000/users/'+this.props.obj.id)
    .then(console.log("deleted"))
    .catch(error => console.log(error))

  }

  render() {
    return (
        <tr>
          <td>
            {this.props.obj.name}
          </td>
          <td>
            {this.props.obj.business_name}
          </td>
          <td>
            {this.props.obj.gst_number}
          </td>
          <td>
          <Link to={"/edit/"+this.props.obj.id} className="btn btn-primary">Edit</Link>
          </td>
          <td>
          <button onClick={this.delete} className="btn btn-danger">Delete</button>
          </td>
        </tr>
    );
  }
}

export default TableRow;